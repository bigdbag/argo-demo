# Argo App Demo - Sangduk Seo

##Done with create-react-app and axios.  Used react-uid to generate unique keys for iterated elements.  Used react-modal to implement modal functionality.  Project was completed in approximately 1.5 hours.  See comments in code for further details beyond the readme.

##Note 1 - CSS/Design in general is an afterthought here, generally aiming to show React component structure and data flow more than design details.  Implemented basic flex CSS details and differing layouts based on media queries to demonstrate responsiveness. 
##Note 2 - Did not implement testing as I don't have enterprise-level experience doing it.  I do have experience in personal projects using jest.
##Note 3 - Decided to implement data fetching as we discussed at onsite interview, from the whiteboarding session.  I changed some parts (for example the method of passing parameters between components) but for the most part I think I can defend this strategy to implement async calls in React applications without the use of redux simply for the purpose of handling async calls.  We have an easy way to access error handling, loading statuses, and passing data between components, and can leverage ternary conditional rendering of various scenarios within components.  

##Live Demo available on S3 here: http://argo-sangdukseo.s3-website.us-east-2.amazonaws.com

##Build - npm build
##Local server - npm start