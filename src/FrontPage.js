import React from 'react';

const FrontPage = ({showQuoteTool}) => {
  return (
    <div className="contentContainer">
      <h2 className="siteTitle">The fastest, easiest way to get a private jet liability insurance quote.</h2>
      <image></image>
      <button onClick={() => showQuoteTool()}>
        Click here to get started
      </button>
    </div>
  );
}

export default FrontPage;
