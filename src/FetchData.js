import { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';

class FetchData extends Component {
  static propTypes = {
    /** parameters object */
    params: PropTypes.any,
    /** request data object */
    requestBody: PropTypes.any,
    /** url path */
    url: PropTypes.string.isRequired,
    /** headers object*/
    headers: PropTypes.any,
    /** CRUD action specification */
    action: PropTypes.string.isRequired,
    /** TO-DO include options such as cors mode, cache mode, referrer, etc. */
  }
  static defaultProps = {
    action: 'get'
  }

  constructor() {
    super();
    this.state = {
      loading: false,
      data: null,
      error: false
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  fetchData = () => {
    this.setState({ loading: true });
    axios({
      method: this.props.action,
      url: this.props.url,
      headers: this.props.headers,
      data: this.props.requestBody,
      params: this.props.params
    })
    .then((response) => {
      this.setState({
        data: response.data,
        loading: false,
        error: false
      })
    })
    .catch((error) => {
        // Error
        if (error.response) {
          error.response.data['errorMessage'] = error.response.data.errorMessage;
          if(error.response.status === 403) {
            error.response.data['errorReasons'] = error.response.data.errors[0].reasons;
          } else if(error.response.status === 422) {
            error.response.data['errorReasons'] = error.response.data.errors[0].reasons.map((field) => {
              return field.reason;
            });
          }
          // The request was made and the server responded with a status code
          // that is not 2xx
          console.log(error.response.data);
          console.log(error.response.status);
          console.log(error.response.headers);
          this.setState({
            data: error.response.data,
            loading: false,
            error: true
          })

        } else if (error.request) {
            this.setState({
              data: {error: "error"},
              loading: false,
              error: true
            })
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest
            console.log(error.request);
        } else {
            this.setState({
              data: {error: "No response received."},
              loading: false,
              error: true
            })
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
        }
        console.log(error.config);
    });
  }

  render() {
    return this.props.children(this.state);
  }
}

export default FetchData;
