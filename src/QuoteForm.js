import React, { Component } from 'react';
import FormSubmission from './FormSubmission';

class QuoteForm extends Component {
  constructor() {
    super();
    this.state = {
      wasFormSubmitted: false,
      formData: {}
    }
    this.form = React.createRef();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    if(this.form.current.reportValidity()) {
      let formattedFormData = {};
      [...event.target.elements].forEach(element => {
        if(element.value !== '') {
          if(element.type === 'number') {
            formattedFormData[element.name] = parseInt(element.value);
          } else {
            formattedFormData[element.name] = element.value;
          }
        }
      })
      this.setState({formData: formattedFormData});
      this.setState({wasFormSubmitted: true});
    }
  }

  render() {
    return (
      <div className="form-container">
        {!this.state.wasFormSubmitted
          ? <div className="form">
              <form ref={this.form} onSubmit={this.handleSubmit}>
                <input placeholder="Enter Owner Name* " required name="owner_name" />
                <select id="jet-select" required name="model">
                  <option value="">Select a Jet</option>
                  <option value="Gulfstream G650">Gulfstream G650</option>
                  <option value="Cessna A-37">Cessna A-37</option>
                  <option value="Dragonfly">Dragonfly</option>
                  <option value="Cessna Citation Encore">Cessna Citation Encore</option>
                </select>
                <input
                  placeholder="Jet Seat Capacity"
                  name="seat_capacity"
                  type="number"
                  required
                  min="1"
                />
                <input 
                  type="date"
                  placeholder="Manufacturing Date"
                  name="manufactured_date"
                  required 
                  max={new Date().toISOString()}
                />
                <input 
                  placeholder="Purchase Price"
                  name="purchase_price"
                  type="number"
                  required
                  min="0"
                />
                <input 
                  placeholder="Broker Email"
                  name="broker_email"
                  type="email"
                  required
                />

                <button type="submit">Get A Quote!</button>
              </form>
            </div>
          : <FormSubmission
              data={this.state.formData}
            />
        }
      </div>
    );
  }
}

export default QuoteForm;
