import React from 'react';
import FetchData from './FetchData';
import QuoteReceipt from './QuoteReceipt';
import {uid} from 'react-uid';

const FormSubmission = (props) => {
  if (!props.data) return null;
  let url = 'https://cors-anywhere.herokuapp.com/j950rrlta9.execute-api.us-east-2.amazonaws.com/v1/ArgoChallenge';
  let headers = {
    'Content-Type': 'application/json',
    'x-api-key': 'L0Q3GvXCwB9jVSmvaJbw5augw4xHCvMy4Egqim2p',
    'X-Requested-With': 'XMLHttpRequest'
  };
  let action = 'post'
  return (
    <FetchData requestBody={props.data} url={url} headers={headers} action={action}>
      {({ loading, data, error }) => {
        return (
          <div className="flex-item">
            {loading
              ? <p className="loadingText"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></p>
              : error
              ? <ul className="loadingText">{data.errorMessage} due to 
                {data.errorReasons.map(function(error){
                  return <li key={uid(error)}>{error}</li>;
                })}
                </ul>
              : data &&
                <QuoteReceipt 
                  data={data}
                />
            }
          </div>
        )
      }}
    </FetchData>
  );
}

export default FormSubmission;
