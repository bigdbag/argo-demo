import React from 'react';
import QuoteForm from './QuoteForm';

const ModalContainer = (props) => {
  return (
    <div className="detailsModal">
      <div className="modalContent">
        <h2>Xavier</h2>
        <QuoteForm></QuoteForm>
      </div>
      <div className="modalFooter">
        <button
          className="closeButton"
          onClick={props.closeModal}
        >Close</button>
      </div>
    </div>
  );
}

export default ModalContainer;
