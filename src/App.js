import React, { Fragment, Component } from 'react';
import Modal from 'react-modal';
import Contact from './Contact';
import FrontPage from './FrontPage';
import ModalContainer from './ModalContainer';
import './App.css';

class App extends Component {
  constructor() {
    super();
    this.state = {
      modalIsOpen: false,
    };
  }

  showQuoteTool = () => {
    this.setState({
      modalIsOpen: true,
    });
  }

  closeModal = () => {
    this.setState({
      modalIsOpen: false,
    })
  }

  render() {
    return (
      <Fragment>
        <div className="container">
          <header>
            Xavier by XYZ Insurance Inc.
          </header>
          <header className="desktop">
            <Contact></Contact>
          </header>
          <main>
            <section>
              <ul className="flex-container">
                <FrontPage
                  showQuoteTool={this.showQuoteTool}>
                </FrontPage>
              </ul>
            </section>
          </main>
          <footer className="mobile">
            <Contact></Contact>
          </footer>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onRequestClose={this.closeModal}
        >
          <ModalContainer
            closeModal={this.closeModal}
          />
        </Modal>
      </Fragment>
    );
  }
}

export default App;
