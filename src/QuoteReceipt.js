import React from 'react';

const QuoteReceipt = (props) => {
  return (
    <div className="quote-receipt">
      <div className="receipt-header">Quote Receipt</div>
      <ul className="receipt-list">
      <li>Annual Premium: {props.data.annual_premium}</li>
      <li>Broker Email: {props.data.quote.broker_email}</li>
      <li>Manufactured Date: {props.data.quote.manufactured_date}</li>
      <li>Model: {props.data.quote.model}</li>
      <li>Owner Name: {props.data.quote.owner_name}</li>
      <li>Purchase Price: {props.data.quote.purchase_price}</li>
      <li>Seat Capacity: {props.data.quote.seat_capacity}</li>
      </ul>
    </div>
  );
}

export default QuoteReceipt;
