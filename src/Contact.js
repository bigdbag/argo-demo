import React from 'react';

const Contact = () => {
  return (
    <div className="contentContainer">
      <h2 className="siteTitle">Contact</h2>
      <section className="contact">
        <a href="mailto:quote@xavier.com" data-rel="external">quote@xavier.com</a><br />
        <a href="tel:1231231234" data-rel="external">123-123-1234</a>
      </section>
    </div>
  );
}

export default Contact;
